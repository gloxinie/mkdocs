# Cascade Informationssysteme GmbH
Seit ihrer Gründung im Jahre 1988 hat sich unsere Firma durch die Entwicklung anspruchsvoller Software für medizinische und technische Anwendungen einen Namen gemacht. Wir haben unsere Leistungsfähigkeit in einer ganzen Reihe von Projekten und Entwicklungen für namhafte Kund:innen unter Beweis gestellt. Einige unserer Programme werden inzwischen weltweit in verschiedenen Sprachen und Betriebssystemen in großer Stückzahl eingesetzt.

## Kernkompetenz
Wir sind spezialisiert auf die Entwicklung skalierbarer, plattformübergreifender Anwendungs- und Systemsoftware mit moderner Benutzer:innenschnittstelle auf der Basis von C/C++ und Qt.

## Projektauswahl
Zahlreiche namhafte medizinische und technische Einrichtungen und Projekte setzen auf Produkte von Cascade Informationssysteme GmbH.

* **BUSDRIVE:** Treiber für Arcnet-Netzwerk- und Multiport-IO-Karten. Hahn-Meitner-Institut, Siemens AG Berlin
* **CROVER:** Bürokommunikation mit redundanten UNIX-Servern. Andersen Consulting, PVWA & Partner Berlin
* **ESPERANZA:** Programmierhilfsmittel zur Mehrprozesssynchronisation in heterogenen Netzwerken. Naica GmbH Berlin
* **FIS:** Fabrikinformationssystem für Fahrradfabrik in Fürstenwalde. WORLDCO Handels GmbH Berlin
* **GENESIS:** Compiler-Generator für attributierte Grammatiken mit SQL-Integration. Naica GmbH Berlin
* **HOPE:** Optimierung verteilter Informationsstrukturen in Krankenhäusern. GSD und COMWARE GmbH Berlin
* **INVITRO:** Automatisierung des nuklearmedizinischen Laboratoriums. Humboldt-Krankenhaus Berlin
* **MEB-AWK:** Auftrags- und Wagenbestandskontrolle. Mercedes Benz AG, Niederlassung Berlin
* **MEB-TEL:** Telefonverwaltung. Mercedes Benz AG, Niederlassungen und Betriebe in Hamburg und Berlin
* **NUKON:** Radionuklidbestands- und -verwendungsnachweis. Charité, Campus Benjamin Franklin Berlin
* **PEFLUKON:** Zugangs- und Strahlenschutzkontrolle für radioaktive Kontrollbereiche. Siemens AG Berlin
* **PHICAD:** CAD-gestützte automatische Überwachung des Schaltplans der Schaltwarte. Philip Morris AG Berlin
* **QUALIMED:** Produktions- und Qualitätskontrolle in der Arzneimittelproduktion. Henning Berlin GmbH
* **WINDEX:** Integrierte Projekt-, Adress- und Dokumentenverwaltung. Berliner Architekten und Bauunternehmen
* **TUBO:** Wartungsoptimierung kombinierter Wasser-, Öl- und Dampfkreisläufe der Berliner Kraftwerke
* **MEB-FAST:** Fahrzeug- und Stellflächenverwaltung. Mercedes Benz AG, Niederlassung Berlin
* **MODIM:** Elektromotorenauswahl für die Projektierung von Fertigungsstraßen. UPM s.r.l. Turin
* **DIRECT-ACCESS:** Workflowsystem für den Software- und CD-ROM-Versandhandel. Direct-Media GmbH, Augsburg


© 1988–2022
