# Impressum

Angaben gemäß § 5 TMG
Cascade Informationssysteme GmbH
Friedrichshaller Straße 17
D-14199 Berlin

Geschäftsleitung: Volker Dörr
Registernummer: HRB Berlin B 28869
Umsatzsteuer-ID: DE136763471
Amtsgericht Charlottenburg Berlin
Telefon: +49 (0) 30-22 37 49 80

### Haftung für Links auf dieser Website
Unsere Webseite enthält Links zu anderen Webseiten, für deren Inhalt wir nicht verantwortlich sind. Haftung für verlinkte Websites besteht für uns nicht, da wir keine Kenntnis rechtswidriger Tätigkeiten hatten und haben, uns solche Rechtswidrigkeiten auch bisher nicht aufgefallen sind und wir Links sofort entfernen werden, sollten uns Rechtswidrigkeiten bekannt werden.
Wenn Ihnen rechtswidrige Links auf unserer Website auffallen, bitten wir Sie, uns zu kontaktieren. Sie finden die Kontaktdaten oben.

### Urheberrechtshinweis
Die von uns erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheber:innenrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheber:innenrechts bedürfen der schriftlichen Zustimmung von uns. Downloads und Kopien dieser Seite sind nur für den privaten, nicht-kommerziellen Gebrauch gestattet. Falls notwendig, werden wir die unerlaubte Nutzung von Teilen der Inhalte unserer Seite rechtlich verfolgen.
Sollten Sie auf dieser Webseite Inhalte finden, die das Urheber:innenrecht verletzen, bitten wir Sie, uns zu kontaktieren.

### Bildernachweis
Die Fotos auf dieser Webseite sind urheberrechtlich geschützt.
Die Bilderrechte liegen bei uns oder den Fotograf:innen, die hier aufgeführt werden.

### Datenschutz
Die Nutzung unserer Webseite ist ohne Angabe personenbezogener Daten möglich.
Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich.
Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten durch Dritte zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit ausdrücklich widersprochen.
Wir behalten uns ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen durch beispielsweise Spam-E-Mails vor.
